import datetime

import requests
from flask import Flask, render_template
import random, threading, webbrowser
from app import convert

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

app = Flask(__name__)
api_key = 'bd5e378503939ddaee76f12ad7a97608'  # https://gist.github.com/SebastianM/d4de7c3427883896b4b8


@app.route("/")
def hello():
    celsius, pressure, description = get_actual_weather()
    get_actual_info()
    table = get_bus_traffic()
    return render_template('main.html', temp=celsius, pressure=pressure, description=description,
                           bus_table=table)


def get_actual_weather():
    city = 'Bratislava'
    res = requests.get('https://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=' + api_key)
    json_object = res.json()
    temp_k = float(json_object['main']['temp'])
    temp_cel = convert.kelvin_to_celsius(temp_k)
    pressure = float(json_object['main']['pressure'])
    description = str(json_object['weather'][0]['description'])
    return temp_cel, pressure, description


def get_actual_info():
    # todo RSS
    return 'nothing'


def get_bus_traffic():
    now = datetime.datetime.now()
    act_date = now.strftime("%d.%m.%Y")
    act_time = now.strftime("%H:%M")
    from_ = 'Zaporožska'
    to_ = 'Zochova'
    #

    res = requests.get(
        "http://imhd.sk/ba/planovac-cesty-vyhladanie-spojenia?z1x=Z%C3%A1poro%C5%BEsk%C3%A1&z1k=3634&z2x=Zochova&z2k=3722&odpr=0&datum=" + act_date + "&cas=" + act_time + "&mnp=2&np=5&ptyp=0&rch=1&dpre=true&dprt=true&dpra=true&dprv=true")
    html_doc = res.text  # the HTML code you've written above
    soup = BeautifulSoup(html_doc, 'html.parser')
    # print(soup.prettify())
    table = soup.find_all("table", "tabulka")
    return table


if __name__ == "__main__":
    port = 5000 + random.randint(0, 999)
    url = "http://127.0.0.1:{0}".format(port)

    threading.Timer(1.25, lambda: webbrowser.open(url)).start()

    app.run(port=port, debug=False)
    # app.run(host='0.0.0.0', port=80, debug=False)
