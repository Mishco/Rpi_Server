def fahrenheit_to_celsius(f):
    # (°F - 32) x 5/9 = °C
    return (f - 32) * 5 / 9


def kelvin_to_celsius(k):
    return format(k - 273.15, '.2f')
